#include <stdio.h>
#include<math.h>
float dist(float x1,float y1,float x2, float y2)
{
    return (sqrt(pow(x2-x1,2)+pow(y2-y1,2)));
}
int main()
{
    float x1,y1,x2,y2;
    printf("enter coordinates x1,y1,x2,y2\n");
    scanf("%f %f %f %f",&x1,&y1,&x2,&y2);
    printf("distance between two points %f,%f and %f,%f is %f",x1,y1,x2,y2,dist(x1,y1,x2,y2));
    return 0;
}