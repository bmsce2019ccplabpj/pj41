#include <stdio.h>
#include <math.h>
int modulus(float d)
{
   int c;
   if(d==0.0)
      c=0;
  else if(d>0.0)
      c=1;
  else
      c=-1;
   return c;    
}
void output(float a,float b,float c,float d,int k)
{
    float r1,r2;
     switch(k)
  {
    case -1: r1 = -b/(2*a);
             r2 = sqrt(fabs(d))/(2*a);
             printf("roots are imaginary \n r1 = %f+i%f\n r2 = %f-i%f\n",r1,r2,r1,r2);
             break;
    case 0: r1 = -b/(2*a);
            printf("roots are equal\n r1 = r2 = %f\n",r1);
            break;
    case 1: r1 = (-b + sqrt(d))/(2*a);
            r2 = (-b - sqrt(d))/(2*a);
            printf("roots are distinct\n r1 = %f \n r2 = %f\n",r1,r2);
    default: printf("invalid entry\n");        
  }
}
int main()
{
  float a,b,c,d;
  int k;
  printf("enter the coefficients of quadratic equation\n");
  scanf("%f %f %f",&a,&b,&c);
  d= b*b - 4*a*c;
  k = modulus(d);
  output(a,b,c,d,k);
  return 0;
}