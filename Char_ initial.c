#include <stdio.h>
int main()
{
    int uc=0,lc=0,num=0;
    char a='0';
    printf("Enter the characters or * to exit\n");
    while(a!='*')
    {
        scanf("%c",&a);
        if(a>='a'&&a<='z')
            lc++;
        else if(a>='A'&&a<='Z')
            uc++;
        else if(a>='0'&&a<='9')
            num++;
    }

    printf("\nNumber of uppercase characters entered = %d \nNumber of lowercase characters entered = %d \n",uc,lc);
    printf("Number of numerical characters entered = %d",num);
    return 0;
}